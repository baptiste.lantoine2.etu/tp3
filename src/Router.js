export default class Router {
	static titleElement;
	static contentElement;
	static routes = [];
	static #menuElement;
	static set menuElement(element) {
		this.#menuElement = element;
		const value = element.querySelectorAll('a');
		value.forEach(elem => {
			console.log(elem.getAttribute('href'));
			//console.log(elem);
			elem.addEventListener('click', event => {
				event.preventDefault();
				Router.navigate(elem.getAttribute('href'));
				console.log('event');
			});
		});
		// au clic sur n'importe quel lien contenu dans "element"
		// déclenchez un appel à Router.navigate(path)
		// où "path" est la valeur de l'attribut `href=".."` du lien cliqué
	}

	static navigate(path) {
		const route = this.routes.find(route => route.path === path);
		if (route) {
			this.titleElement.innerHTML = `<h1>${route.title}</h1>`;
			this.contentElement.innerHTML = route.page.render();
			route.page.mount?.(this.contentElement);
		}
	}
}
